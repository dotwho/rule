const fs = require("fs");
const path = require("path");

const loonPath = "../../../public/loon";
const CategoryPath = "../../../public/category";

async function getPayLoad(name) {
  return new Promise((resolve) => {
    let url = path.join(`${loonPath}/${name}.list`);
    const file = fs.readFileSync(url, { encoding: "utf-8" });
    const data = file.split("\n");
    resolve(data);
  });
}

function sortRules(ary) {
  const KEYWORD = [];
  const PROCESS = [];
  const DOMAIN = [];
  const SUFFIX = [];
  const CIDR = [];
  const CIDR6 = [];
  const IPS = [];
  const temp = Array.from(ary).sort();
  temp.forEach((rule) => {
    const item = rule.replace(/\s*/g, "");
    if (item !== "") {
      if (item.indexOf("PROCESS-NAME,") > -1) {
        PROCESS.push(item);
      } else if (item.indexOf("DOMAIN-KEYWORD,") > -1) {
        KEYWORD.push(item);
      } else if (item.indexOf("DOMAIN,") > -1) {
        DOMAIN.push(item);
      } else if (item.indexOf("DOMAIN-SUFFIX,") > -1) {
        SUFFIX.push(item);
      } else if (item.indexOf("IP-CIDR,") > -1) {
        CIDR.push(item);
      } else if (item.indexOf("IP-CIDR6,") > -1) {
        CIDR6.push(item);
      } else {
        if (item.indexOf("#") === -1) {
          IPS.push(item);
        }
      }
    }
  });
  return [
    ...PROCESS,
    ...KEYWORD,
    ...DOMAIN,
    ...SUFFIX,
    ...CIDR,
    ...CIDR6,
    ...IPS,
  ];
}

function sortFull(ary) {
  const temp = Array.from(ary).sort();
  const normal = [];
  const full = [];
  temp.forEach((item) => {
    if (item.indexOf("full:") > -1) {
      full.push(item);
    } else {
      normal.push(item);
    }
  });
  return [...normal, ...full];
}

module.exports = {
  async getListLoad(names) {
    const loads = [];
    names.forEach(async (name) => {
      loads.push(getPayLoad.apply(this, [name]));
    });
    return new Promise((resolve) => {
      Promise.all(loads).then((res) => {
        resolve(res.flat());
      });
    });
  },
  getDocumentFiles(document) {
    const ary = [];
    const dirPath = `../${document}/`;
    const files = fs.readdirSync(dirPath);
    files.forEach((file) => {
      if (file.indexOf(".list") > -1) {
        const paths = path.join(dirPath, file);
        const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
        const data = filePath.split("\n");
        const res = []
        data.forEach((item) => {
          if (item) {
            res.push(item.replace("+.", "."))
          }
        })
        if (res.length > 0) {
          ary.push(...res);
        }
      }
    });
    return Array.from(new Set(ary)).sort();
  },
  saveFile(name, payload) {
    const temp = sortRules(payload);
    let paths = path.join(`${loonPath}/${name}.list`);
    const base = Buffer.from(temp.join("\n"));
    fs.writeFileSync(paths, base, "utf8");
  },
  saveCateFile(name, payload) {
    const temp = sortFull(payload);
    let paths = path.join(`${CategoryPath}/${name}`);
    const base = Buffer.from(temp.join("\n"));
    fs.writeFileSync(paths, base, "utf8");
  },
  deduplication(payload1, payload2) {
    const after = [];
    payload1.forEach((item) => {
      if (!payload2.includes(item)) {
        after.push(item);
      }
    });
    return after;
  },
};
