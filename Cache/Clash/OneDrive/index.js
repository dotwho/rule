const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("OneDrive");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
    "Disney",
    "Developer",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("OneDrive", payload);
}

init();
