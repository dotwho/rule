const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Netflix");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Netflix", payload);
}

init();
