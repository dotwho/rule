const fs = require("fs");
const path = require("path");
const Utils = require("../../utils");

const dirPath = "./";
const files = fs.readdirSync(dirPath);

let ary = [];
files.forEach((file) => {
  if (file.indexOf(".list") > -1) {
    const paths = path.join(dirPath, file);
    const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
    const data = filePath.split("\n")
    if (data) {
      data.forEach((item) => {
        if (item && item.indexOf("#") === -1 && item.indexOf("!") === -1) {
          if (item.lastIndexOf('.') === item.length - 1) {
            const xz = item.substr(0, item.length - 1)
            ary.push(xz)
          } else {
            ary.push(item);
          }
        }
      })
    }
  }
});

// ary = ary.map((r) => r.replace('+.', ''));

const temp = Array.from(new Set(ary));

const index = Math.ceil(temp.length / 2);
const file1 = temp.splice(0, index);
const file2 = temp.splice(0, index);

Utils.saveFile("Advertising", file1);
Utils.saveFile("Privacy", file2);
