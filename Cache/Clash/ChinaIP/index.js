const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("ChinaIP", ".txt");
  Utils.saveFile("ChinaIP", files);
}

init();
