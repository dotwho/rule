const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Disney");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Disney", payload);
}

init();
