const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Global");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
    "Disney",
    "Developer",
    "OneDrive",
    "Xbox",
    "Nintendo"
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Global", payload);
}

init();
