const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("UnBreak");
  Utils.saveFile("UnBreak", files);
}

init();
