const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Developer");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
    "Disney",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Developer", payload);
}

init();
