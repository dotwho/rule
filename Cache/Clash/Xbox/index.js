const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Xbox");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
    "Disney",
    "Developer",
    "OneDrive"
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Xbox", payload);
}

init();
