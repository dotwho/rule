const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("Nintendo");
  const otherRules = await Utils.getYAMLLoad([
    "UnBreak",
    "Advertising",
    "Netflix",
    "Disney",
    "Developer",
    "OneDrive",
    "Xbox",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("Nintendo", payload);
}

init();
