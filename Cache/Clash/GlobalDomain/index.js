const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("GlobalDomain", ".txt");
  const otherRules = await Utils.getYAMLLoad([
    "Advertising",
    "Privacy",
    "ChinaDomain"
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("GlobalDomain", payload);
}

init();
