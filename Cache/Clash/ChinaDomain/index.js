const Utils = require("../../utils");

async function init() {
  const files = Utils.getDocumentFiles("ChinaDomain", ".txt");
  const otherRules = await Utils.getYAMLLoad([
    "Advertising",
    "Privacy",
  ]);
  const payload = Utils.deduplication(files, otherRules);
  Utils.saveFile("ChinaDomain", payload);
}

init();
