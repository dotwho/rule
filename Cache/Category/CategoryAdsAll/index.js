const fs = require("fs");
const path = require("path");
const Utils = require("../../utils");

const dirPath = "./";
const files = fs.readdirSync(dirPath);


const removeDomain = ["ilce.alicdn.com"]

function isDelete(str) {
  return removeDomain.find(item => str.indexOf(item) > -1);
}

const ary = [];
files.forEach((file) => {
  if (file.indexOf(".list") > -1) {
    const paths = path.join(dirPath, file);
    const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
    const data = filePath.split("\n");
    if (data) {
      data.forEach((item) => {
        if (
          item &&
          !isDelete(item) &&
          item.indexOf("#") === -1 &&
          item.indexOf("!") === -1 &&
          item.indexOf("DOMAIN-KEYWORD") === -1 &&
          item.indexOf("IP-CIDR") === -1 &&
          (item.indexOf("DOMAIN,") > -1 || item.indexOf("DOMAIN-SUFFIX,") > -1)
        ) {
          let temp = item;
          temp = temp.replace("DOMAIN-SUFFIX,", "");
          temp = temp.replace("DOMAIN,", "full:");
          ary.push(temp);
        }
      });
    }
  }
});

const temp = Array.from(new Set(ary));

Utils.saveCateFile("category-ads-all", temp);
