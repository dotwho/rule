const fs = require("fs");
// https://bitbucket.org/dotwho/rule/raw/master/ChinaList.txt
const paths = './ChinaList.txt';
const files = fs.readFileSync(paths, { encoding: "utf-8" });
const data = files.split("\n")

let temp = data.map((item) => {
  // const index = item.lastIndexOf(']')
  // const domain = item.substr(0, index + 1)
  // if (
  //   item.includes('[/qq.com/]')
  //   || item.includes('[/jd.com/]')
  //   || item.includes('[/gtimg.com/]')
  //   || item.includes('[/idqqimg.com/]')
  //   || item.includes('[/weixin.com/]')
  //   || item.includes('smtcdns')
  //   || item.includes('wechat')
  //   || item.includes('servicewechat')
  //   || item.includes('weixinbridge')
  //   || item.includes('cdntips')
  //   || item.includes('tencent')
  // ) {
  //   return `${domain}https://1.12.12.12/dns-query`
  // }
  // return `${domain}192.168.1.1`
  return item.replace('https://1.12.12.12/dns-query', '192.168.1.1')
})

temp.push('[/qpic.com/]192.168.1.1')
temp.push('[/qlogo.com/]192.168.1.1')
temp.push('[/apple.com/]192.168.1.1')
temp.push('[/apple-dns.net/]192.168.1.1')
temp.push('[/apple-cloudkit.com/]192.168.1.1')
temp.push('[/safebrowsing.apple/]192.168.1.1')
temp.push('[/live.com/]192.168.1.1')
temp.push('[/msecnd.net/]192.168.1.1')
temp.push('[/microsoft.com/]192.168.1.1')
temp.push('[/microsoftonline.com/]192.168.1.1')
temp.push('[/partex.com/]192.168.1.1')
temp.push('[/one.edu.kg/]192.168.1.1')
temp.push('[/steamstatic.com/]192.168.1.1')

temp.sort()
temp = Array.from(new Set(temp));

temp.unshift('[/cn/]192.168.1.1')

temp.push('https://pz.fyi/dns-query')
temp.push('https://dns.seia.io/dns-query')
temp.push('https://d.niaox.com/dns-query')
temp.push('https://dns.flymc.cc/dns-query')
// temp.push('https://adg.geili.me/dns-query')
// temp.push('https://sunnygyl.com/dns-query')


// temp.push('https://doh.apad.pro/dns-query')
// https://101.101.101.101/dns-query
// https://223.5.5.5/dns-query

const base = Buffer.from(temp.join('\n'));
fs.writeFileSync('../../public/ChinaList.txt', base, "utf8");
