const fs = require("fs");
const path = require("path");

const publicPath = "../../../public/loon";

function setRuleHeader(header, ary) {
  return ary.map((a) => {
    if (a.indexOf(":") > -1) {
      return `${header}6,${a}`
    }
    return `${header},${a}`
  });
}

module.exports = {
  getRuleFiles(document) {
    let domain = [];
    let domain_suffix = [];
    let domain_keyword = [];
    let domain_regex = [];
    let ip_cidr = [];
    const savePath = path.join(`${publicPath}/${document}.list`);
    const dirPath = `../${document}/`;
    const files = fs.readdirSync(dirPath);
    files.forEach((file) => {
      if (file.indexOf(".json") > -1) {
        const paths = path.join(dirPath, file);
        const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
        const rules = JSON.parse(filePath).rules;
        let index = 0;
        if (rules.length > 1) {
          index = 1;
        }
        const data = rules[index];
        Object.keys(data).forEach((key) => {
          if (key === "domain") {
            domain.push(...setRuleHeader("DOMAIN", data[key]));
          } else if (key === "domain_suffix") {
            domain_suffix.push(...setRuleHeader("DOMAIN-SUFFIX", data[key]));
          } else if (key === "domain_keyword") {
            domain_keyword.push(...setRuleHeader("DOMAIN-KEYWORD", data[key]));
          } else if (key === "domain_regex") {
            domain_regex.push(...setRuleHeader("URL-REGEX", data[key]));
          } else if (key === "ip_cidr") {
            ip_cidr.push(...setRuleHeader("IP-CIDR", data[key]));
          }
        });
      }
    });
    domain = [...new Set(domain)];
    domain_suffix = [...new Set(domain_suffix)];
    domain_keyword = [...new Set(domain_keyword)];
    domain_regex = [...new Set(domain_regex)];
    ip_cidr = [...new Set(ip_cidr)];
    const payload = [
      ...domain,
      ...domain_suffix,
      ...domain_keyword,
      ...domain_regex,
      ...ip_cidr,
    ].sort().join("\n");
    fs.writeFileSync(savePath, payload, {
      encoding: "utf8",
    });
  },
};
