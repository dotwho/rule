cd ./Proxy
wait

curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/proxy.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/telegramip.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/netflix.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/netflixip.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/ai.json
wait

node ./index.js
echo 'Proxy Completed'
wait
