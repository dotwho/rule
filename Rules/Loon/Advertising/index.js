const fs = require("fs");
const path = require("path");

const dirPath = "./";
const publicPath = "../../../public/loon";
const files = fs.readdirSync(dirPath);

const ary = [];
files.forEach((file) => {
  if (file.indexOf(".list") > -1) {
    const paths = path.join(dirPath, file);
    const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
    const data = filePath.split("\n");
    if (data) {
      data.forEach((item) => {
        if (item && item.indexOf("#") === -1 && item.indexOf("!") === -1) {
          ary.push(item);
        }
      });
    }
  }
});

const temp = Array.from(new Set(ary)).sort().join("\n");
const savePath = path.join(`${publicPath}/Advertising.list`);
fs.writeFileSync(savePath, temp, {
  encoding: "utf8",
});
