const fs = require("fs");
const path = require("path");

const SingPath = "../../../public/singbox";

module.exports = {
  getRuleFiles(document) {
    let domain = [];
    let domain_suffix = [];
    let domain_keyword = [];
    let domain_regex = [];
    let ip_cidr = [];
    const dirPath = `../${document}/`;
    let savePath = path.join(`${SingPath}/${document}.json`);
    const files = fs.readdirSync(dirPath);
    files.forEach((file) => {
      if (file.indexOf(".json") > -1) {
        const paths = path.join(dirPath, file);
        const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
        const rules = JSON.parse(filePath).rules;
        let index = 0;
        if (rules.length > 1) {
          index = 1;
        }
        const data = rules[index];
        Object.keys(data).forEach((key) => {
          if (key === "domain") {
            domain.push(...data[key]);
          } else if (key === "domain_suffix") {
            domain_suffix.push(...data[key]);
          } else if (key === "domain_keyword") {
            domain_keyword.push(...data[key]);
          } else if (key === "domain_regex") {
            domain_regex.push(...data[key]);
          } else if (key === "ip_cidr") {
            ip_cidr.push(...data[key]);
          }
        });
      }
    });
    domain = [...new Set(domain)].sort();
    domain_suffix = [...new Set(domain_suffix)].sort();
    domain_keyword = [...new Set(domain_keyword)].sort();
    domain_regex = [...new Set(domain_regex)].sort();
    ip_cidr = [...new Set(ip_cidr)].sort();
    const objectData = {};
    if (domain.length > 0) {
      objectData["domain"] = domain;
    }
    if (domain_suffix.length > 0) {
      objectData["domain_suffix"] = domain_suffix;
    }
    if (domain_keyword.length > 0) {
      objectData["domain_keyword"] = domain_keyword;
    }
    if (domain_regex.length > 0) {
      objectData["domain_regex"] = domain_regex;
    }
    if (ip_cidr.length > 0) {
      objectData["ip_cidr"] = ip_cidr;
    }
    const fileData = {
      "version": 3,
      "rules": [objectData]
    }
    fs.writeFileSync(savePath, JSON.stringify(fileData, null, 2), {
      encoding: "utf8",
    });
  }
};
