cd ./proxy
wait

curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/proxy.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/refs/heads/sing-box-ruleset/appletv.json
wait
curl -O https://raw.githubusercontent.com/DustinWin/ruleset_geodata/sing-box-ruleset/telegramip.json
wait

node ./index.js
wait

echo 'Proxy Completed'
