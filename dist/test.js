const fs = require("fs");
const path = require("path");

const date = Date.now();

const dirPath = "./dist/";
const files = fs.readdirSync(dirPath);

const ary = [];
files.forEach((file) => {
  if (file.indexOf(".txt") > -1) {
    const paths = path.join(dirPath, file);
    const filePath = fs.readFileSync(paths, { encoding: "utf-8" });
    const data = filePath.split("\n")
    if (data) {
      ary.push(...data);
    }
  }
});

console.log("Rules:", ary.length);

const repeatData = ary.filter(
  (item, index, self) => self.indexOf(item) !== index
);

console.log("Repeats:", repeatData.length);

console.log("Time:", `${Math.floor((Date.now() - date) / 1000)}s`);

fs.writeFileSync("./dist/repeat.md", repeatData.join("\n"), {
  encoding: "utf8",
});
