let a =
  "dHJvamFuOi8vYzdlYjNmYmYtZTc2ZjBAY2RuLnh4eHh4LnRvcDoyMDAwMT9hbGxvd0luc2VjdXJlPTEmcGVlcj1jZG4xLmFsaWJhYmEuY29tJnNuaT1jZG4yLmFsaWJhYmEuY29tI2FiY2R0aXRsZQ0Kc3M6Ly9ZMmhoWTJoaE1qQXRhV1YwWmkxd2IyeDVNVE13TlRwalltRXlOams0TXc9PUB4eC54eC54eDozOTAxNiNhYmNkdGl0bGUyDQp2bWVzczovL2V5SjJJam9pTWlJc0luQnpJam9pWEhVM05tWTBYSFU0Wm1SbElERWlMQ0poWkdRaU9pSjRlQzU0ZUM1MGIzQWlMQ0p3YjNKMElqb2lPREFpTENKcFpDSTZJbU5pWVRJMk9UZ3pJaXdpWVdsa0lqb2lNQ0lzSW5OamVTSTZJbUYxZEc4aUxDSnVaWFFpT2lKM2N5SXNJblI1Y0dVaU9pSnViMjVsSWl3aWFHOXpkQ0k2SW5oNExuaDRMblJ2Y0NJc0luQmhkR2dpT2lKY0wzZHpQMlZrUFRJMU5qQWlMQ0owYkhNaU9pSWlMQ0ptY0NJNkltTm9jbTl0WlNKOQ==";

function getUrlParams2(url) {
  const urlStr = url.split("?")[1];
  const urlSearchParams = new URLSearchParams(urlStr);
  const result = Object.fromEntries(urlSearchParams.entries());
  return { url: url.split("?")[0], params: result };
}

const proxy = [];
const proxies = atob(a).split("\r\n");

proxies.forEach((node) => {
  if (node) {
    // {
    //   tag: "abcdtitle";
    //   server: "cdn.xxxxx.top";
    //   server_port: 20001;
    //   type: "trojan";
    //   password: "c7eb3fbf-e76f0";
    //   tls: {
    //     enabled: true;
    //     insecure: true;
    //     server_name: "cdn.alibaba.com";
    //   }
    // }
    if (node.indexOf("trojan://") === 0) {
      const item = node.split("#");
      const result = getUrlParams2(item[0]);
      const host = result.url.replace("trojan://", "");
      const temp = host.split("@");
      const temp2 = temp[1].split(":");
      proxy.push({
        tag: decodeURIComponent(item[1]),
        server: temp2[0],
        server_port: Number(temp2[1]),
        type: "trojan",
        password: temp[0],
        tls: {
          enabled: result.params.allowInsecure === "1" ? true : false,
          insecure: true,
          server_name: result.params.sni,
        },
      });
    }
    // {
    //   tag: "abcdtitle2";
    //   server: "xx.xx.xx";
    //   server_port: 39016;
    //   type: "shadowsocks";
    //   method: "chacha20-ietf-poly1305";
    //   password: "cba26983";
    // }
    if (node.indexOf("ss://") === 0) {
      const host = node.replace("ss://", "");
      const temp = host.split("@");
      const temp2 = atob(temp[0]);
      const temp3 = temp[1].split("#");
      const temp4 = temp3[0].split(":");
      proxy.push({
        tag: decodeURIComponent(temp3[1]),
        server: temp4[0],
        server_port: Number(temp4[1]),
        type: "shadowsocks",
        method: temp2.split(":")[0],
        password: temp2.split(":")[1],
      });
    }
    // {
    //   tag: "直连 1";
    //   server: "xx.xx.top";
    //   server_port: 80;
    //   type: "vmess";
    //   uuid: "cba26983";
    //   security: "auto";
    //   network: "tcp";
    //   alter_id: 0;
    //   transport: {
    //     type: "ws";
    //     path: "/ws?ed=2560";
    //     headers: {
    //       host: "xx.xx.top";
    //     }
    //   }
    // }
    if (node.indexOf("vmess://") === 0) {
      const vmess = node.replace("vmess://", "");
      const temp = JSON.parse(atob(vmess));
      proxy.push({
        tag: decodeURIComponent(temp.ps),
        server: temp.host,
        server_port: Number(temp.port),
        type: "vmess",
        uuid: temp.id,
        security: temp.scy,
        network: "tcp",
        alter_id: Number(temp.aid),
        transport: {
          type: temp.net,
          path: temp.path,
          headers: {
            host: temp.add,
          },
        },
      });
    }
  }
});

console.log(proxy)
console.log(proxy.length)
