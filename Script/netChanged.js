/*
  设置全局运行模式
  0:Global Direct
  1:By Rule
  2:Global Proxy
*/
function getArgs() {
  return Object.fromEntries(
    $argument
      .split("&")
      .map((item) => item.split("="))
      .map(([k, v]) => [k, decodeURIComponent(v)])
  );
}

let args = getArgs();
const wifi = args.wifi.split('/')

const conf = JSON.parse($config.getConfig());

if (wifi.includes(conf.ssid)) {
  $config.setRunningModel(0)
  $notification.post(conf.ssid,"直连","")
} else {
  $config.setRunningModel(1)
  $notification.post(conf.ssid ? conf.ssid : "蜂窝","分流", "")
}
